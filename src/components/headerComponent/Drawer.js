import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import Button from "@mui/material/Button";
import Link from '@mui/material/Link';
import MenuOutlinedIcon from '@mui/icons-material/MenuOutlined';
import SearchIcon from '@mui/icons-material/Search';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import './drawer.css'
import { Container, Grid } from "@mui/material";
import FacebookOutlinedIcon from '@mui/icons-material/FacebookOutlined';
import TwitterIcon from '@mui/icons-material/Twitter';
import PinterestIcon from '@mui/icons-material/Pinterest';
import InstagramIcon from '@mui/icons-material/Instagram';

export default function TemporaryDrawer(props) {
    const [state, setState] = React.useState({
        left: false
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (
            event.type === "keydown" &&
            (event.key === "Tab" || event.key === "Shift")
        ) {
            return;
        }

        setState({ ...state, [anchor]: open });
    };

    const list = (anchor) => (
        <Box className="box"
            sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 330 }}
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <Container>
                <Grid container>
                    <Grid item xs={12} sm={12} className="header-1">
                        <Link href="/login"
                        >
                            LOGIN
                        </Link>
                    </Grid>
                </Grid>
                <Grid container style={{ marginTop: "30px" }}>
                    <Grid item xs={12} style={{ display: "flex", justifyContent: "space-evenly", margin: "0 25px" }}>
                        <SearchIcon />
                        <FavoriteBorderIcon />
                        <ShoppingCartOutlinedIcon />
                    </Grid>
                </Grid>
            </Container>
            <Grid container style={{marginTop:"10px"}}>
                <Grid className='header-2' item xs={12} sm={12}>
                    <Link href="/">
                        Home
                    </Link>
                    <Link href="/products">
                        Shop
                    </Link>
                    <Link href="/products/:product-id">
                        Shop Details
                    </Link>
                    <Link href="/products/shopping-cart">
                        Shopping Cart
                    </Link>
                    <Link href="/products/check-out">
                        Check Out
                    </Link>
                    <Link href="/blog-page">
                        Blog
                    </Link>
                    <Link href="/contact-us">
                        Contacts
                    </Link>
                    <p>
                    Free shipping, 30-day return or refund guarantee.
                    </p>
                    <div className="drawer-social">
                    <Link href="https://www.facebook.com/">
                        <FacebookOutlinedIcon className="icon" style={{color:"#1A77F2"}}/>
                    </Link>
                    <Link href="https://twitter.com/">
                        <TwitterIcon className="icon" style={{color:"#1D9BF0"}}/>
                    </Link>
                    <Link href="https://www.pinterest.com/">
                        <PinterestIcon className="icon" style={{color:"#E60022"}}/>
                    </Link>
                    <Link href="https://www.instagram.com/">
                        <InstagramIcon className="icon" style={{color:"#FE0179"}}/>
                    </Link>
                </div>
                </Grid>
                
            </Grid>
        </Box>
    );

    return (
        <div>
            {["left"].map((anchor) => (
                <React.Fragment key={anchor}>
                    <Button onClick={toggleDrawer(anchor, true)}><MenuOutlinedIcon /></Button>
                    <Drawer
                        anchor={anchor}
                        open={state[anchor]}
                        onClose={toggleDrawer(anchor, false)}
                    >
                        {list(anchor)}
                    </Drawer>
                </React.Fragment>
            ))}
        </div>
    );
}
