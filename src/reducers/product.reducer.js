
import { GET_PRODUCT, NUMBER_PRODUCT } from "../constants/product.const";

const initialState = {
    a : "a",
    numberProduct: 0,
    carts: [],
    productArr: []
}

export default function productReducer(state = initialState, action) {
    
        switch (action.type) {
            case NUMBER_PRODUCT:
                state.numberProduct = action.payload.length;
                state.carts = action.payload;
                break;
            case GET_PRODUCT:
                state.productArr = action.payload;
                break;
            default:
                break;
        }
        console.log(state);
    return {...state};
}
