import { GET_PRODUCT, NUMBER_PRODUCT, SET_PAGE } from "../constants/product.const";


export const countNumberProduct = (e) => {
    return async (action) => {

        await action({
            type: NUMBER_PRODUCT,
            payload: e
        })
    }
}
export const setPage = (data) => {
    return {
        type: SET_PAGE,
        payload: data
    }
}
export const getAllProduct = (condition) => {
    return async (dispatch) => {
        try {
            
            var requestOptions = {
                method: 'GET',
                headers: { "Content-Type": "application/json" },
                redirect: 'follow'
            };
            const res = await fetch(process.env.REACT_APP_API + "/api/products" + `?${condition}`, requestOptions)
            const data = await res.json();

            await dispatch({
                type: GET_PRODUCT,
                payload: data.Product
            })
        } catch (error) {

        }


    }
}